DROP DATABASE IF EXISTS Kafeine;
CREATE DATABASE IF NOT EXISTS Kafeine;
CREATE TABLE IF NOT EXISTS Products(

id_product INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
product_name VARCHAR(100) NOT NULL,
product_type VARCHAR(50) NOT NULL,
brand VARCHAR(25) NOT NULL, 
origin VARCHAR(50),
product_weight FLOAT,
price FLOAT NOT NULL,
expiration DATE,
lot VARCHAR(20) NOT NULL,
quantity INT NOT NULL,
img VARCHAR(50) NOT NULL,
active BOOLEAN DEFAULT TRUE

);

CREATE TABLE IF NOT EXISTS users(

id_user INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
username VARCHAR(20) NOT NULL UNIQUE,
user_password VARCHAR(16) NOT NULL,
email VARCHAR(100) NOT NULL UNIQUE

);


INSERT INTO Products (product_name,product_type,brand,origin,product_weight,price,expiration,lot,quantity,img) VALUES ('Gran Aroma Mezcla','Descafeinado','Marcilla','Mexico',0.5,6.3,'2022-12-05','689574',7,'cafe-saimaza.jpg');