    <?php

    class Database{

        protected $host = "localhost";
        protected $username = "root";
        protected $password = "";
        protected $database = "kafeine";
        protected $conexion;

        public function __construct(){
    
            $this->conexion = new mysqli($this->host,$this->username,$this->password,$this->database);
            if ($this->conexion->connect_errno) {
                echo "Falló la conexión con MySQL: (" . $this->conexion->connect_errno . ") " . $this->conexion->connect_error;
            }
            mysqli_set_charset($this->conexion,'utf8');
        }

        public function getConexion(){
            return $this->conexion;
        }

        public function fetch($query){
            $stm = $this->conexion->prepare($query);
            $stm->execute();
            $consulta = $stm->get_result();
            if($row = $consulta->fetch_assoc()){
                return $row;
            }
               

        }
        public function insert($query){
            $stm = $this->conexion->prepare($query);
            if($stm->execute()){
                return "EXITO";
            }else{
                return "ERROR";
            }
        }
        
        public function update($query){
            $stm = $this->conexion->prepare($query);
            if($stm->execute()){
                return "Producto actualizado con éxito";
            }else{
                return "NO SE HA ACTUALIZADO";  
            }
        }
       
        public function disconnect(){
            $this->conexion->close();
        }
    }
?>   