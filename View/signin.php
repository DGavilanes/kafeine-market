
<?php require_once'components/Navbar.php'?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/login.css">
    <link rel="stylesheet" href="css/sidenav.css">
    <title>Registro</title>
</head>
<body>

<?php $nav = new Navbar();
     $nav->displayLogo();
?>

    <div class="container mt-5">
        <div class="row">
            <div class="col-xs-12 col-md-6 mx-auto my-5">

                <form action="../Controller/log-sign/checkSignin.php" method="POST" class="border p-3"> 
                    <div class="form-group">   
                        <input type="text" name="username" class="form-control" placeholder="Nombre" required>
                    </div>
                    <div class="form-group">
                        <input type="text" name="password" class="form-control" placeholder="Constraseña" pattern="^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,16}$" title="La contraseña debe contener entre 8 y 16 caracteres al menos una mayúscula y al menos un número"
                        required>
                        
                    </div>
                    <div class="form-group">   
                        <input type="text" name="confirmPassword"class="form-control" placeholder="Confirmar Constraseña" pattern="^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,16}$"
                        title="La contraseña debe contener entre 8 y 16 caracteres al menos una mayúscula y al menos un número" required>
                    </div>
                    <div class="form-group">   
                        <input type="text"  name="email" class="form-control" placeholder="Email" pattern="^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$"
                        title="ejemplo: micorreo@correo.com" required>
                    </div>
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary mb-2">Registrarse</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</body>
</html>