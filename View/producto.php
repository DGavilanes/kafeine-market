<?php

require_once 'components/Navbar.php';
require_once 'components/Sidenav.php';
require_once '../Model/Database.php';
require_once '../Controller/Product.php';

$id = $_GET['id'];
session_start();


$con = new Database();
$row = $con->fetch("SELECT * FROM products WHERE id_product = $id");
$item = new Product($row['product_name'],$row['product_type'],$row['brand'],$row['origin'],$row['product_weight'],
$row['price'],$row['expiration'],$row['lot'],$row['quantity'],$row['img'],$row['active']);
$navbar = new Navbar();
$sidenav = new Sidenav()

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/login.css">
    <link rel="stylesheet" href="css/sidenav.css">
    <title><?php echo $item->getproductName()?></title>
</head>

<body>
    
<?php echo $sidenav->display(); 
            if(isset($_SESSION['username'])){
                if($_SESSION['username'] == 'admin'){
                    echo $navbar->adminNavbar();}
                else{
                    echo $navbar->UserNavbar();
                }
            }else{
                echo $navbar->noUserNavbar();
            };?>

    <div class="container">
           
        <div class="row">
            
            <div class="col-12 col-md-6 mt-5 text-center">
                
                <img class="img-fluid product-img" src="assets/img/<?php echo $item->getImg() ?>" alt="<?php echo $item->getproductName()?>"  >
                
            </div>
            <div class="col-12 col-md-6 mt-xl-5">
                    <h4 class="mt-5"><?php echo $item->getproductName()?></h4>
                    <div class="linea mb-3"></div>
                    <p class="my-3 h3">Origen: <?php echo $item->getOrigin()?></p>
                    <?php   if(strtolower($item->getproductType())=="capsulas"){
                                echo '<p class="my-3 h3">Cantidad: '.$item->getproductWeight().'uds</p>';
                            }else{
                                echo '<p class="my-3 h3">Peso: '.$item->getproductWeight().'Kg</p>';
                        }?>
                    <p class="my-3 h3">Precio: <span class="text-danger"><?php echo $item->getPrice()?>€</span></p>
                    <p class="mt-3 h3">Disponibilidad: 
                        <?php if($item->getQuantity() > 0){
                        echo '<span class="text-success">En Stock</span>';
                    }else{
                        echo '<span class="text-danger">No disponible</span>';
                    }?></p>
            </div>
        </div>
        <div class="row justify-content-end my-3">
            <div class="col-xs-12 col-md-6">
                <div class="row">

                    <div class="col-6  d-flex justify-content-around">

                        <button class="quantity-minus-button btn btn-danger"><i class="fa fa-minus"></i></button>
                        <input class="form-control" type="text" id="quantity" min="1" max="50" value="1">
                        <button class="quantity-plus-button btn btn-success"><i class="fa fa-plus"></i></button>

                    </div>

                    <div class="col-6  text-right">
                        <button type="submit " class="btn btn-primary">Añadir al carro <i class="fa fa-shopping-cart"></i></button>
                    </div>
                </div>
            </div>

        </div>

    </div>




    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="../Controller/js/quantityForm.js"></script>
</body>

</html>