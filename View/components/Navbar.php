<?php

class Navbar
{

    public function noUserNavbar()
    {

        return '<div class="bg-danger">
                    <div class="container d-flex flex-row justify-content-between">
                        <a class="navbar-brand py-3 text-white" href="http://localhost/Proyectos/Kafeine_Market/index.php"><img src="http://localhost/Proyectos/Kafeine_Market/View/assets/icons/logo-white.svg" alt="Kafeine-Maket-Logo"></a>
                        <div class="d-flex flex-row">
                            <button class="btn shadow-none trigger-btn"><a href="#login" data-toggle="modal"><i class="fa fa-user-circle fa-2x m-4 text-white"></i></a></button>
                            <button class="btn shadow-none"><a href="WIP.php"><i class="fa fa-shopping-cart fa-2x m-4 text-white"></i></a></button>
                        </div>
                    </div>
                </div>

            <div id="login" class="modal fade" style="display: none;">
                    <div class="modal-dialog modal-login">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Log In</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                            <div id="error-msg" class="alert alert-danger d-none" role="alert">Email o contraseña incorrectas</div>
                                <form method="post" id="form">
                                    <div class="form-group">
                                        <i class="fa fa-user text-dark"></i>
                                        <input type="text" name="email" id="email" class="form-control" placeholder="Email" required="required">
                                    </div>
                                    <div class="form-group">
                                        <i class="fa fa-lock text-dark"></i>
                                        <input type="password" name="password" id="password" class="form-control" placeholder="Password" required="required">
                                    </div>
                                    <div class="form-group text-right">
                                        <input type="submit" id="submit" class="col-4 btn btn-primary btn-lg" value="Login">
                                    </div>
                                </form>

                            </div>
                        <div class="modal-footer">
                            <p>No tienes cuenta? Registrate <a href="View\signin.php">aquí</a></p>

                    </div>
                </div>
            </div>
        </div> ';
    }


    public function adminNavbar(){
        return '<div class="bg-danger">
                    <div class="container d-flex flex-row justify-content-between">
                        <a class="navbar-brand py-3 text-white" href="http://localhost/Proyectos/Kafeine_Market/index.php"><img src="http://localhost/Proyectos/Kafeine_Market/View/assets/icons/logo-white.svg" alt="Kafeine-Maket-Logo"></a>
                            <div class="d-flex flex-row">
                            
                            <form action="http://localhost/Proyectos/Kafeine_Market/Controller/log-sign/checkLogout.php">
                                <button class="btn shadow-none trigger-btn" type="submit"><i class="fa fa-user fa-2x m-4 text-white"></i></button>
                            </form>
                                <button class="btn shadow-none"><a href="http://localhost/Proyectos/Kafeine_Market/View/admin.php"><i class="fa fa-wrench fa-2x m-4 text-white"></i></a></button>
                            
                            </div>
                    </div>
                </div>';
    }

    public function userNavbar(){
        return '<div class="bg-danger">
                    <div class="container d-flex flex-row justify-content-between">
                        <a class="navbar-brand py-3 text-white" href="http://localhost/Proyectos/Kafeine_Market/index.php"><img src="http://localhost/Proyectos/Kafeine_Market/View/assets/icons/logo-white.svg" alt="Kafeine-Maket-Logo"></a>
                            <div class="d-flex flex-row">
                                <form action="http://localhost/Proyectos/Kafeine_Market/Controller/log-sign/checkLogout.php">
                                    <button class="btn shadow-none trigger-btn" type="submit"><i class="fa fa-user fa-2x m-4 text-white"></i></button>
                                </form>
                                <button class="btn shadow-none"><a href="#"><i class="fa fa-shopping-cart fa-2x m-4 text-white"></i></a></button>
                            
                            </div>
                    </div>
                </div>';

    }
    public function displayLogo(){

        return '<div class="bg-danger">
                <div class="container d-flex flex-row justify-content-center">
                     <a class="navbar-brand py-3 text-white" href="http://localhost/Proyectos/Kafeine_Market/index.php"><img src="http://localhost/Proyectos/Kafeine_Market/View/assets/icons/logo-white.svg" alt="Kafeine-Maket-Logo"></a>
                </div>
             </div>';

    } 
}

