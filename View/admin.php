<?php

require_once 'components/Navbar.php';
require_once '../Model/Database.php';
require_once '../Controller/Product.php';
$navbar = new Navbar;
session_start();

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/login.css">
    <link rel="stylesheet" href="css/sidenav.css">
    <title>Admin</title>
</head>

<body>

     <?php echo $navbar->displayLogo() ?>

    <div class="container">
        <div class="row">

            <div class="col-12 p-4 text-right">

                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
                    Crear
                </button>

                <div class="modal fade text-left" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLongTitle">Crear Producto row <?php ?></h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form action="../Controller/createProduct.php" method="post">
                                    <div class="form-group">
                                        <input type="text" name="product_name" id="product_name" class="form-control" required placeholder="Nombre del producto">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="product_type" id="product_type" class="form-control" placeholder="Tipo de producto">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="brand" id="brand" class="form-control" placeholder="Marca">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="origin" id="origin" class="form-control" placeholder="Origen">
                                    </div>
                                    <div class="form-group">
                                        <input type="number" name="price" id="price" class="form-control" placeholder="Precio" step="0.01">
                                    </div>
                                    <div class="form-group">
                                        <input type="number" name="product_weight" id="product_weight" class="form-control" placeholder="Tamaño/Cantidad" step="0.01">
                                    </div>
                                    <div class="form-group">
                                        <input type="number" name="lot" id="lot" class="form-control" placeholder="Lote">
                                    </div>
                                    <div class="form-group">
                                        <input type="number" name="quantity" id="quantity" class="form-control" placeholder="Cantidad">
                                    </div>
                                    <div class="form-group">
                                        <input type="date" name="expiration" id="expiration" class="form-control" placeholder="Fecha de Caducidad">
                                    </div>
                                    <div class="custom-file">
                                        <input type="file" name ="imagen" class="custom-file-input" id="customFileLang" lang="es">
                                        <label class="custom-file-label" for="customFileLang">Seleccionar Archivo</label>
                                    </div>

                                    <div class="modal-footer">

                                        <button type="submit" class="btn btn-primary">Crear</button>

                                    </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>



            <table class="table table-hover table-responsive">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Producto</th>
                        <th scope="col">Tipo</th>
                        <th scope="col">Marca</th>
                        <th scope="col">Origen</th>
                        <th scope="col" class="text-right">Precio</th>
                        <th scope="col" class="text-right">Tamaño</th>
                        <th scope="col" class="text-right">Lote</th>
                        <th scope="col" class="text-right">Cantidad</th>
                        <th scope="col" class="text-right" data-toggle="tooltip" data-placement="left" title="format: dd-mm-yy">Caducidad</th>
                        <th scope="col">Editar</th>
                        <th scope="col">Borrar</th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    
                    $con = new Database();

                    for ($i = 1; $i < 50; $i++) {

                        $row = $con->fetch("SELECT * FROM products where id_product = $i");
                        $item = new Product(
                            $row['product_name'],
                            $row['product_type'],
                            $row['brand'],
                            $row['origin'],
                            $row['product_weight'],
                            $row['price'],
                            $row['expiration'],
                            $row['lot'],
                            $row['quantity'],
                            $row['img'],
                            $row['active']
                        );
                        $item->setId($i);
                            
                        if (isset($row['id_product']) && $row['active'] == 1) {
                            $id = $row['id_product'];
                            echo '<tr>
                                    <th scope="row">' . $i . '</th>
                                    <td>' . $item->getproductName() . '</td>
                                    <td>' . $item->getproductType() . '</td>
                                    <td>' . $item->getBrand() . '</td>
                                    <td>' . $item->getOrigin() . '</td>
                                    <td class="text-right">' . $item->getPrice() . '</td>
                                    <td class="text-right">' . $item->getproductWeight() . '</td>
                                    <td class="text-right">' . $item->getLot() . '</td>
                                    <td class="text-right">' . $item->getQuantity() . '</td>';
                            if ($item->getExpiration() === NULL) {
                                echo '<td class="text-center"></td>';
                            } else {

                                echo '<td>' . date("d/m/y", strtotime($item->getExpiration())) . '</td>';
                            };
                            echo '
                                    <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editarProducto'.$id.'" ><i class="fa fa-pencil"></i></button></td>
                                    <td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#eliminarProducto'.$id.'"><i class="fa fa-trash"></i></button></td>
                                </tr>';
                        }
                            echo '<div class="modal fade" id="editarProducto'.$id.'" tabindex="-1" role="dialog" aria-labelledby="editarProducto'.$id.'" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLongTitle">Editar Producto</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form action="../Controller/updateProduct.php?id='.$id.' " method="post">
                                                    
                                                            <div class="form-group">                        
                                                                <input type="text" name="updated_product_name" id="updated_product_name" class="form-control"  value="'.$item->getproductName().'" placeholder="Nombre del Producto">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="updated_product_type" id="updated_product_type" class="form-control"  value="'.$item->getproductType().'" placeholder="Tipo de Producto">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="updated_brand" id="updated_brand" class="form-control"  value="'.$item->getBrand().'" placeholder="Marca">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="updated_origin" id="updated_origin" class="form-control"  value="'.$item->getOrigin().'" placeholder="Origen">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="updated_price" id="updated_price" class="form-control"  value="'.$item->getPrice().'" placeholder="Precio">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="updated_product_weight" id="updated_product_weight" class="form-control"  value="'.$item->getproductWeight().'" placeholder="Peso">
                                        </div>
                                        <div class="form-group">
                                            <input type="number" name="updated_lot" id="updated_lot" class="form-control"  value="'.$item->getLot().'" placeholder="Lote">
                                        </div>
                                        <div class="form-group">
                                            <input type="number" name="updated_quantity" id="updated_quantity" class="form-control"  value="'.$item->getQuantity().'" placeholder="Cantidad">
                                        </div>
                                        <div class="form-group">
                                            <input type="date" name="updated_date" id="updated_date" class="form-control"  value="'.$item->getExpiration().'" placeholder="Fecha de caducidad">
                                        </div>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" name="imagen" id="customFileLang" lang="es">
                                            <label class="custom-file-label" for="customFileLang">Seleccionar Archivo</label>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-primary">Actualizar</button>
                                        </div>
                            
                                    </form>
                            </div>      
                        </div>
                    </div>
                </div>
                </div>

                <div class="modal fade" id="eliminarProducto'.$id.'" tabindex="-1" role="dialog" aria-labelledby="eliminarProducto'.$id.'" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Eliminar Producto</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <span>Estas seguro de que quires borrar el siguiente producto?</span>
                            <form action="../Controller/deleteProduct.php?id='.$id.'" method="post" class="d-flex justify-content-end mt-5">
                                <button class="btn">No borrar</button>
                                <button class="btn btn-danger">Borrar</button>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>';

                    }
                    ?>

                </tbody>
            </table>

        



            <!-- Boton de Borrar -->


        

        <!-- <script src="../js/functions.js"></script> -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <script src="../Controller/js/functions.js"></script>
</body>

</html>