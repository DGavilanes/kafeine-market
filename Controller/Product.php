<?php


    class Product   {

        protected $id_product,$product_name ,$product_type, $brand, $origin, $product_weight,$price,$expiration, $lot, $quantity, $img, $active;
        
        public function __construct($product_name,$product_type,$brand,$origin,$product_weight,$price, $expiration,$lot,$quantity,$img,$active){
            $this->product_name= $product_name;
            $this->product_type = $product_type;
            $this->brand = $brand;
            $this->origin = $origin;
            $this->product_weight = $product_weight;
            $this->price = $price;
            $this->expiration = $expiration;
            $this->lot = $lot;
            $this->quantity = $quantity;
            $this->img = $img;
            $this->active = $active;
        }

        
        public function getID(){
            return $this->id_product;
        }
        public function setID($id_product){
            $this->id_product = $id_product;
        }
        public function getproductName(){
            return $this->product_name;
        }
        public function setproductName($product_name){
            $this->product_name = $product_name;
        }

        public function getproductType(){
            return $this->product_type;
        }
        public function setproductType($product_type){
            $this->product_type = $product_type;
        }
        public function getBrand(){
            return $this->brand;
        }
        public function setBrand($brand){
            $this->brand = $brand;
        }
        public function getOrigin(){
            return $this->origin;
        }
        public function setOrigin($origin){
            $this->origin = $origin;
        }
        public function getproductWeight(){
            return $this->product_weight;
        }
        public function setproductWeight($product_weight){
            $this->product_weight = $product_weight;
        }
        public function getPrice(){
            return $this->price;
        }
        public function setPrice($price){
            $this->price = $price;
        }
        public function getExpiration(){
            return $this->expiration;
        }
        public function setExpiration($expiration){
            $this->expiration = $expiration;
        }
        public function getLot(){
            return $this->lot;
        }
        public function setLot($lot){
            $this->lot = $lot;
        }
        public function getQuantity(){
            return $this->quantity;
        }
        public function setQuantity($quantity){
            $this->quantity = $quantity;
        }
        public function getImg(){
            return $this->img;
        }
        public function setImg($img){
            $this->img = $img;
        }
        public function getActive(){
            return $this->active;
        }
        public function setActive($active){
            $this->active = $active;
        }

        public function displayProduct($name,$img,$id){
            return '<li class="col-12 col-md-6 col-xl-4 my-2">
            <div class="card mx-auto border-0" style="width: 17rem" >
                <a href="View/producto.php?id='.$id.'"><img src="View/assets/img/'.$img.'" class="card-img-top img-fluid " alt="..."></a>
                <div class="card-body text-center">
                  <p class="card-text">'.$name.'</p>
            </div>
        </li>';
        }
    }

    