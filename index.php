<?php

    require_once 'View/components/Navbar.php';
    require_once 'View/components/Sidenav.php';
    require_once 'Model/Database.php';
    require_once 'Controller/Product.php';

    session_start();
    
    $navbar = new Navbar(); 
    $sidenav = new Sidenav();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="View/css/style.css">
    <link rel="stylesheet" href="View/css/login.css">
    <link rel="stylesheet" href="View/css/sidenav.css">

  

    <title>Kafeine Market</title>
</head>
<body>
    
       
        <?php echo $sidenav->display(); 
            if(isset($_SESSION['username'])){
                if($_SESSION['username'] == 'admin'){
                    echo $navbar->adminNavbar();}
                else{
                    echo $navbar->UserNavbar();
                }
            }else{
                echo $navbar->noUserNavbar();
            };?>
            

    <div class="container bc-container w100">

          <?php if(!empty($_SESSION['username'])){
              echo '<h3 class="d-flex justify-content-center">Hola, '.$_SESSION['username'].'</h3>';
          }  ?>
    <nav class="navbar navbar-expand-lg navbar-light my-4 justify-content-end">

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-center" id="navbarNavAltMarkup">
            <div class="navbar-nav text-right">
                <a class="nav-item nav-link text-dark" href="#">Café Natural</a>
                <a class="nav-item nav-link text-dark" href="#">Café Molido</a>
                <a class="nav-item nav-link text-dark" href="#">Café Descafeinado</a>
                <a class="nav-item nav-link text-dark" href="#">Cápsulas</a>
                <a class="nav-item nav-link text-dark" href="#">Accesorios</a>

            </div>
        </div>
    </nav> 


        <div class="row">
            <ul class="d-flex flex-wrap justify-content-between">
                <?php
                    $conexion = new Database();
                    for($i = 1; $i< 7; $i++){
                        $prod = $conexion->fetch("SELECT * FROM products WHERE id_product = $i");
                        $item = new Product($prod['product_name'],$prod['product_type'],$prod['brand'],$prod['origin'],$prod['product_weight'],
                        $prod['price'],$prod['expiration'],$prod['lot'],$prod['quantity'],$prod['img'],$prod['active']);
                        echo $item->displayProduct($item->getproductName(),$item->getImg(),$prod['id_product']); 
                    }
                    $conexion->disconnect();
                ?>
            </ul>
        </div>


           
     
    </div>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>        
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="Controller/js/login.js"></script>                

                    

</body>

</html>

